﻿using NotPad.BaseStream;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotPad
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PAD fgp = new PAD();
            fgp.MdiParent = this;
            fgp.Show();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int count = this.MdiChildren.Length;
            if (count == 0)
            {
                return;
            }
            DialogResult result = saveFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                string filepath = saveFileDialog1.FileName;
                SecuentialStream ss = new SecuentialStream(filepath);
                Form activechild = this.ActiveMdiChild;
                TextBox txtarea = (TextBox)activechild.Controls[0];
                ss.writeText(txtarea.Text);
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult)
            {

            }

        }
    }
}
