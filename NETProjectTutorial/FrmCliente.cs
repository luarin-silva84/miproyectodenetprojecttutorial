﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {
        private DataTable tblCliente;
        private DataRow drCliente;

        

        public DataRow DrCliente
        {

            set
            {
                drCliente = value;
                maskedId.Text = drCliente["Id"].ToString();
                maskedCedula.Text = drCliente["Cedula"].ToString();
                txtNombre.Text = drCliente["Nombres"].ToString();
                txtApellido.Text = drCliente["Apellidos"].ToString();
                txtDireccion.Text = drCliente["Direccion"].ToString();
            }
        }

        public DataTable TblCliente
        {
            set
            {
                tblCliente = value;
            }
        }

        public FrmCliente()
        {
            InitializeComponent();
           
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string  cedula, nombre, apellido, direccion;
           string telefono;

           
            cedula = maskedCedula.Text;
            nombre = txtNombre.Text;
            apellido = txtApellido.Text;
            direccion = txtDireccion.Text;
            telefono = maskedTelefono.Text;



            if (drCliente != null)
            {
                DataRow drnew = tblCliente.NewRow();

                int index = tblCliente.Rows.IndexOf(drCliente);
                drnew["Id"] = drCliente["Id"];
                drnew["Cedula"] = cedula;
                drnew["Nombre"] = nombre;
                drnew["Apellido"] = apellido;
                drnew["Direccion"] = direccion;
                drnew["Telefono"] = telefono;
                drnew["NA"] = nombre + "" + apellido;

                tblCliente.Rows.RemoveAt(index);
                tblCliente.Rows.InsertAt(drnew, index);


            }
            else
            {
                int id = tblCliente.Rows.Count + 1;
                tblCliente.Rows.Add(id,  cedula, nombre, apellido, direccion, telefono);
                MessageBox.Show("Se ha agregado perfectamente", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            Dispose();
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            if (drCliente != null)
            {
                maskedCedula.Text = drCliente["Cedula"].ToString();
                txtNombre.Text = drCliente["Nombres"].ToString();
                txtApellido.Text = drCliente["Apellidos"].ToString();
                maskedTelefono.Text = drCliente["Telefono"].ToString();
                maskedcorreo.Text = drCliente["Correo"].ToString();
                txtDireccion.Text = drCliente["Direccion"].ToString();

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
