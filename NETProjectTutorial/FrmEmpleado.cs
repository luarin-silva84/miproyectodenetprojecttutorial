﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmEmpleado : Form { 

        private DataTable tblEmpleados;
        private DataSet dsEmpleados;
        private DataRow drEmpleados;
        private BindingSource bsEmpleados;
       

        public DataSet DsEmpleados
        {
           
            set
            {
                dsEmpleados = value;
            }
        }

        public DataRow DrEmpleados
        {

            set
            {
                drEmpleados = value;
                maskedId.Text = drEmpleados["Id"].ToString();
                maskedInss.Text = drEmpleados["Inss"].ToString();
                maskedCedula.Text = drEmpleados["Cedula"].ToString();
                txtNombre.Text = drEmpleados["Nombre"].ToString();
                txtApellido.Text = drEmpleados["Apellido"].ToString();
                txtDireccion.Text = drEmpleados["Direccion"].ToString();

            }
        }

        public DataTable TblEmpleados
        {
           
            set
            {
                tblEmpleados = value;
            }
        }

        public FrmEmpleado()
        {
            InitializeComponent();
            bsEmpleados = new BindingSource();
        }



        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void FrmEmpleado_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string id, inss, cedula, nombre, apellido, direccion;
            int telefono, celular;
            double salario;
            var sexo = this.cmbSexo.GetItemText(this.cmbSexo.SelectedItem);
            MessageBox.Show(sexo);

            id = maskedId.Text;
            inss = maskedInss.Text;
            cedula = maskedCedula.Text;
            nombre = txtNombre.Text;
            apellido = txtApellido.Text;
            direccion = txtDireccion.Text;
            telefono = Convert.ToInt32(maskedTelefono.Text);
            celular = Convert.ToInt32(maskedCelular.Text);
            salario = Convert.ToDouble(maskedSalario.Text);

            if (drEmpleados != null)
            {
                DataRow drnew = tblEmpleados.NewRow();

                int index = tblEmpleados.Rows.IndexOf(drEmpleados);
                drnew["Id"] = drEmpleados["Id"];
                drnew["Inss"] = inss;
                drnew["Cedula"] = cedula;
                drnew["Nombre"] = nombre;
                drnew["Apellido"] = apellido;
                drnew["Direccion"] = direccion;
                drnew["Telefono"] = telefono;
                drnew["Celular"] = celular;
                drnew["sexo"] = sexo;
                drnew["Salario"] = salario;

                tblEmpleados.Rows.RemoveAt(index);
                tblEmpleados.Rows.InsertAt(drnew, index);


            }
            else
            {
               tblEmpleados.Rows.Add(tblEmpleados.Rows.Count + 1, id, inss, cedula, nombre, apellido, direccion, telefono, celular, sexo, salario);
            }

            Dispose();


        }
        private void FrmEmpleado_Load()
        {
            bsEmpleados.DataSource = dsEmpleados;
            bsEmpleados.DataMember = dsEmpleados.Tables["Empleado"].TableName;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
