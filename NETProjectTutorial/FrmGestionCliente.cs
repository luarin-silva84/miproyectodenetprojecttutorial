﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionCliente : Form
    {
        private DataSet dsCliente;
        private BindingSource bsCliente;

        public DataSet DsCliente
        {

            set
            {
                dsCliente = value;
            }
        }

        public FrmGestionCliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            FrmCliente fe = new FrmCliente();
            fe.TblCliente= dsCliente.Tables["Cliente"];
            fe.ShowDialog();
        }

        private void FrmGestionCliente_Load(object sender, EventArgs e)
        {
            bsCliente.DataSource = dsCliente;
            bsCliente.DataMember = dsCliente.Tables["Cliente"].TableName;
            dgvClientes.DataSource = bsCliente;
            dgvClientes.AutoGenerateColumns = true;
        }

        private void txtFinder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsCliente.Filter = string.Format("Id like '*{0}*' or nombres like '*{0}*' or cedula like '*{0}*' ", txtFinder.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvClientes.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow x = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmCliente fp = new FrmCliente();
            fp.TblCliente = dsCliente.Tables["Cliente"];
            
            fp.DrCliente = x;
            fp.ShowDialog();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            DataGridViewSelectedRowCollection rowCollection = dgvClientes.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                dsCliente.Tables["Empleado"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
