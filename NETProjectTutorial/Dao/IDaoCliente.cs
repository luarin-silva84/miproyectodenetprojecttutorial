﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.Dao
{
    interface IDaoCliente : IDao<Cliente>
    {
        Cliente findById(int id);
        Cliente findByCedula(string cedula);
        List<Cliente> findByApellidos(string apellidos);
    }
}
