﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NETProjectTutorial.Properties;
using NETProjectTutorial.entities;

namespace NETProjectTutorial
{
    public partial class FrmProducto : Form
    {
        private DataTable tblProductos ;
        private DataSet dsProductos;
        private BindingSource bsProductos;
        private DataRow drProducto;

        public FrmProducto()
        {
            InitializeComponent();
            bsProductos = new BindingSource();
        }


        
        public DataSet DsProductos { set { dsProductos = value; } }
        public DataRow DrProducto
        {
            set {
                drProducto = value;
                txtSku.Text =  drProducto["Sku"].ToString();
                txtName.Text = drProducto["Nombre"].ToString();
                txtDesc.Text = drProducto["Descripcion"].ToString();
                nmQuant.Value = Int32.Parse(drProducto["Cantidad"].ToString());
                msktxtPri.Text = drProducto["Precio"].ToString();
            }
            
        }

         public DataTable TblProductos
        {

            set
            {
                tblProductos = value;
            }
        }

       

        void btnSave_Click(object sender, EventArgs e)
        {
            string sku, nombres, descripcion;
            int cantidad;
            double precio;

            sku = txtSku.Text;
            nombres = txtName.Text;
            descripcion = txtDesc.Text;
            cantidad = (int)nmQuant.Value;
            bool result = Double.TryParse(msktxtPri.Text, out precio);
            
            if(drProducto != null)
            {
                DataRow drNew = tblProductos.NewRow();

                int index = tblProductos.Rows.IndexOf(drProducto);
                drNew["Id"] = drProducto["Id"];
                drNew["Sku"] = sku;
                drNew["Nombre"] = nombres;
                drNew["Descripcion"] = descripcion;
                drNew["Cantidad"] = cantidad;
                drNew["Precio"] = precio;


                tblProductos.Rows.RemoveAt(index);
                tblProductos.Rows.InsertAt(drNew, index);               

            }
            else
            {
                tblProductos.Rows.Add(tblProductos.Rows.Count + 1, sku, nombres, descripcion, cantidad, precio);
            }

            Dispose();

        }

        private void FrmProducto_Load(object sender, EventArgs e)
        {
            bsProductos.DataSource = dsProductos;
            bsProductos.DataMember = dsProductos.Tables["Producto"].TableName;       
             

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
