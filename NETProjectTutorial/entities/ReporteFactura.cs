﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class ReporteFactura
    {
        private string codFactura;
        private DateTime fecha;
        private double subTotal;
        private double iva;
        private double total;
        private int cantidad;
        private double precio;
        private string sku;
        private string nombre_Producto;
        private string nombre_Empleado;
        private string apellido_Empleado;

        public string CodFactura
        {
            get
            {
                return codFactura;
            }

            set
            {
                codFactura = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public double SubTotal
        {
            get
            {
                return subTotal;
            }

            set
            {
                subTotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public string Sku
        {
            get
            {
                return sku;
            }

            set
            {
                sku = value;
            }
        }

        public string Nombre_Producto
        {
            get
            {
                return nombre_Producto;
            }

            set
            {
                nombre_Producto = value;
            }
        }

        public string Nombre_Empleado
        {
            get
            {
                return nombre_Empleado;
            }

            set
            {
                nombre_Empleado = value;
            }
        }

        public string Apellido_Empleado
        {
            get
            {
                return apellido_Empleado;
            }

            set
            {
                apellido_Empleado = value;
            }
        }

        public ReporteFactura(string codFactura, DateTime fecha, double subTotal, double iva, double total, int cantidad, double precio, string sku, string nombre_Producto, string nombre_Empleado, string apellido_Empleado)
        {
            this.CodFactura = codFactura;
            this.Fecha = fecha;
            this.SubTotal = subTotal;
            this.Iva = iva;
            this.Total = total;
            this.Cantidad = cantidad;
            this.Precio = precio;
            this.Sku = sku;
            this.Nombre_Producto = nombre_Producto;
            this.Nombre_Empleado = nombre_Empleado;
            this.Apellido_Empleado = apellido_Empleado;
        }
    }
}
