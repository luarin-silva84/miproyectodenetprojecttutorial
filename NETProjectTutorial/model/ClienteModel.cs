﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ClienteModel
    {
        private  List<Cliente> ListCliente = new List<Cliente>();
        private Implement.DaoImplementsCliente daocliente;
        
        public ClienteModel()
        {
            daocliente = new Implement.DaoImplementsCliente();
        }

        public  List<Cliente> GetListCliente()
        {
            return daocliente.findAll();
        }

        public  void Populate()
        {
            Cliente[] clientes =
              {
             new Cliente(001,"401-230698-0002F","Gabriel","Perez","88999775","gabriel@yahoo.es","Managua,barrio Boer"),
            new Cliente(002,"401-250798-0003G","Juan","Perez","88945675","gabriel@yahoo.es","Managua,Memorial Sandino"),
            new Cliente(003,"401-230899-3335H","Carlos","Perez","88947875","gabriel@yahoo.es","Managua,Distrito Uno"),
             };


        }
    }
}
