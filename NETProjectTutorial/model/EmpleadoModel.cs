﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> Lstempleados = new List<Empleado>();

        public static List<Empleado> GetLstEmpleado()
        {
            return Lstempleados;
        }

        public static void Populate()
        {
           // Empleado[] empleados =
             //   {
               // new Empleado(1,"1658574-2","001-110288-5588D","Pepito","Perez","Del arbolito 2C. abajo","22558877","88774455",SEXO.f,25145.33),
                //new Empleado(2,"1655555-2","001-012408-5588D","Ana","Conda","De la Racachaca 1C. al Sur","22667755","88564775",SEXO.m,30158.33),
                //new Empleado(3,"1888888-2","001-010485-5588D","Armando","Cas","Del Pali 1C. arriba","22888877","78544455",SEXO.m,40565.33),
           // };

            Lstempleados = JsonConvert.DeserializeObject<List<Empleado>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.MOCK_DATA));
        }
    }
}
