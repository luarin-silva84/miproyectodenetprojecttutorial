﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmFactura : Form
    {
        private DataSet dsSistema;
        private BindingSource bsProductoFactura;
        private bool canUpdate = true;
        private bool needUpdate = false;
        private double subtotal;
        private double iva;
        private double total;
        private string cod_factura;

        public DataSet DsSistema
        {

            set
            {
                dsSistema = value;
            }
        }

        public FrmFactura()
        {
            InitializeComponent();
            bsProductoFactura = new BindingSource();
        }

       

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void FrmFactura_Load(object sender, EventArgs e)
        {
            cmbEmpleados.DataSource = dsSistema.Tables["Empleado"];
            cmbEmpleados.DisplayMember = "NA";
            cmbEmpleados.ValueMember = "Id";

            cmbProductos.DataSource = dsSistema.Tables["Producto"];
            cmbProductos.DisplayMember = "SKUN";
            cmbProductos.ValueMember = "Id";

            cmbCliente.DataSource = dsSistema.Tables["Cliente"];
            cmbCliente.DisplayMember = "NA";
            cmbCliente.ValueMember = "Id";

            dsSistema.Tables["ProductoFactura"].Rows.Clear();
            bsProductoFactura.DataSource = dsSistema.Tables["ProductoFactura"];
            dgvProductoFactura.DataSource = bsProductoFactura;

            cod_factura = "FA" + dsSistema.Tables["Factura"].Rows.Count + 1;
            txtCodigo.Text = cod_factura;
            
        }

        private void cmbProductos_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow row = ((DataRowView)cmbProductos.SelectedItem).Row;
            Console.WriteLine(cmbProductos.SelectedValue);
            txtCantidad.Text = row["Cantidad"].ToString();
            txtPrecio.Text = row["Precio"].ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow drProducto = ((DataRowView)cmbProductos.SelectedItem).Row;
                DataRow drProductoFactura = dsSistema.Tables["ProductoFactura"].NewRow();
                drProductoFactura["Id"] = drProducto["Id"];
                drProductoFactura["SKU"] = drProducto["SKU"];
                drProductoFactura["Nombre"] = drProducto["Nombre"];
                drProductoFactura["Cantidad"] = 1;
                drProductoFactura["Precio"] = drProducto["Precio"];
                dsSistema.Tables["ProductoFactura"].Rows.Add(drProductoFactura);


            }
            catch (ConstraintException)
            {
                MessageBox.Show(this, "ERROR ,Producto ya agregado, verifique por favor!", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvProductoFactura_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow dgrPrdocutoFactura = dgvProductoFactura.Rows[e.RowIndex];
            DataRow drProductoFactura= ((DataRowView)dgrPrdocutoFactura.DataBoundItem).Row;

            DataRow drPoducto = dsSistema.Tables["Producto"].Rows.Find(drProductoFactura["Id"]);

            if (Int32.Parse(drProductoFactura["Cantidad"].ToString()) > Int32.Parse(drPoducto["Cantidad"].ToString()))
            {
                MessageBox.Show(this, "ERROR, la cantidad de producto a vender no puede ser mayor que la del almacen", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                drProductoFactura["Cantidad"] = Int32.Parse(drPoducto["Cantidad"].ToString());
            }
            CalcularTotalFactura();
        }

        private void CalcularTotalFactura()
        {
            subtotal = 0;
            foreach (DataRow dr in dsSistema.Tables["ProductoFactura"].Rows)
            {
                subtotal += Int32.Parse(dr["Cantidad"].ToString()) * Double.Parse(dr["Precio"].ToString());
            }

            iva = subtotal * 0.15;
            total = iva + subtotal;

            txtSubtotal.Text = subtotal.ToString();
            txtIva.Text = iva.ToString();
            txtTotal.Text = total.ToString();

        }

        private void dgvProductoFactura_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            CalcularTotalFactura();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection dgrProductoFactura = dgvProductoFactura.SelectedRows;
            if (dgrProductoFactura.Count == 0)
            {
                MessageBox.Show(this, "ERROR, no hay filas para eliminar!!", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow dgvRow = dgrProductoFactura[0];
            DataRow drProductoFactura = ((DataRowView)dgrProductoFactura[0].DataBoundItem).Row;

            DialogResult result = MessageBox.Show("Seguro que quiere eliminar esta fila", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (result == DialogResult.Yes)
            {
                dsSistema.Tables["ProductoFactura"].Rows.Remove(drProductoFactura);
            }
            
            
        }

        private void UpdateData()
        {
            if (cmbEmpleados.Text.Length > 1)
            {

                List<Empleado> searchData = dsSistema.Tables["Empleado"].AsEnumerable().Select(
                    dataRow =>
                    new Empleado
                    {
                        Id = dataRow.Field<Int32>("Id"),
                        Nombres = dataRow.Field<String>("Nombres"),
                        Apellidos = dataRow.Field<String>("Apellidos")
                    }).ToList();

                HandleTextChanged(searchData.FindAll(e => e.Nombres.Contains(cmbEmpleados.Text)));
            }
            else
            {
                RestartTimer();
            }
        }


        //Actualizar el combo con nuevos datos
        private void HandleTextChanged(List<Empleado> dataSource)
        {
            var text = cmbEmpleados.Text;

            if (dataSource.Count() > 0)
            {
                cmbEmpleados.DataSource = dataSource;

                var sText = cmbEmpleados.Items[0].ToString();
                cmbEmpleados.SelectionStart = text.Length;
                cmbEmpleados.SelectionLength = sText.Length - text.Length;
                cmbEmpleados.DroppedDown = true;
                return;
            }
            else
            {
                cmbEmpleados.DroppedDown = false;
                cmbEmpleados.SelectionStart = text.Length;
            }
        }

        private void RestartTimer()
        {
            timer1.Stop();
            canUpdate = false;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            canUpdate = true;
            timer1.Stop();
            UpdateData();

        }

        private void cmbEmpleados_TextChanged(object sender, EventArgs e)
        {
            if (needUpdate)
            {
                if (canUpdate)
                {
                    canUpdate = false;
                    UpdateData();
                }
                else
                {
                    RestartTimer();
                }
            }


        }

        private void cmbEmpleados_SelectedIndexChanged(object sender, EventArgs e)
        {
            needUpdate = false;
        }

        private void cmbEmpleados_TextUpdate(object sender, EventArgs e)
        {
            needUpdate = true;

        }

        private void btnFacturar_Click(object sender, EventArgs e)
        {
            if (dsSistema.Tables["ProductoFactura"].Rows.Count == 0)
            {
                MessageBox.Show(this, "ERROR, No se puede generar la factura, revise que hayan productos", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataRow drFactura = dsSistema.Tables["Factura"].NewRow();
            drFactura["CodFactura"] = cod_factura;
            drFactura["Fecha"] = DateTime.Now;
            drFactura["Observaciones"] = txtObserv.Text;
            drFactura["Empleado"] = cmbEmpleados.SelectedValue;
            drFactura["Subtotal"] = subtotal;
            drFactura["Iva"] = iva;
            drFactura["Total"] = total;
            drFactura["Cliente"] = cmbCliente.SelectedValue;

            dsSistema.Tables["Factura"].Rows.Add(drFactura);

            foreach (DataRow dr in dsSistema.Tables["ProductoFactura"].Rows)
            {
                DataRow drDetalleFactura = dsSistema.Tables["DetalleFactura"].NewRow();
                drDetalleFactura["Factura"] =drFactura["Id"];
                drDetalleFactura["Producto"] = dr["Id"];
                drDetalleFactura["Cantidad"] = dr["Cantidad"]; 
                drDetalleFactura["Precio"] = dr["Precio"];
                dsSistema.Tables["DetalleFactura"].Rows.Add(drDetalleFactura);

                DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(dr["Id"]);
                drProducto["Cantidad"] = Double.Parse(drProducto["Cantidad"].ToString()) - Double.Parse(dr["Cantidad"].ToString());

            }

            FrmReporteFactura rp = new FrmReporteFactura();
            rp.MdiParent = this.MdiParent;
            rp.DsSistema = dsSistema;
            rp.Show();
            Dispose();
           

            

        }
    }
}
